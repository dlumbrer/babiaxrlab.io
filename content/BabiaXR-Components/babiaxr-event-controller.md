---
linktitle: "babiaxr-event-controller"
title: "babiaxr-event-controller Component"
#date: 2020-09-23T15:06:09+02:00
draft: false
weight: 100
categories: [ "Components", "Documentation" ]
tags: ["api", "demo"]
---

This component manages the events between the `Navigation bar` and the `chart` component. You need it if you use a UI navigation bar component.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/ui_nav_bar/simplebar_chart_with_ui/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| navigation         | ID of the UI navigation bar that will manage the chart   | JSON  stringfied (list of objects) | - |
| target          | ID of the chart that will change its data  | JSON stringfied (list of objects) | - |

```html
<a-entity babiaxr-event-controller = 'navigation: navigationbarid;
          target: chartid'></a-entity>
```
