---
linktitle: "babiaxr-filterdata"
title: "babiaxr-filterdata Component"
#date: 2020-09-23T15:15:36+02:00
draft: false
weight: 140
categories: [ "Components", "Documentation" ]
tags: ["api", "demo"]
---

This component must be used with one of the `babiaxr-querier` components. This component will select a part of the data retrieved (by a key/filter) in order to represent just that part of the data. If the filter is not defined, it will retrieve all the data.
This component will put the data selected into the `babiaData` attribute of the entity.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts_querier/simplebar_chart_querier/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| from            | Id of one of the querier components  | string | - |
| filter (Optional)        | Key of the item that you want to analyse, this key must be in the data retrieved from a querier. (ex. `name=David`) | string   | - |

### Guides:
- [How to use querier/filter/vismapper components](../../docs/how_to_use_querier_filter_vismapper/)