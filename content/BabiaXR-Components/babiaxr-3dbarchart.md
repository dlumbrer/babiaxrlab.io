---
linktitle: "babiaxr-3dbarchart"
#date: 2020-06-19T12:58:05+02:00
title: babiaxr-3Dbarchart Component
draft: false
weight: 30
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---
This component must be used with one of the [`babiaxr-vismapper`](../babiaxr-vismapper) components, with the `x-axis`, `z-axis` and `height` attribute defined.

This component shows a 3D bar chart.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts/3dbars_chart/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| legend          | Shows a legend when hovering a bar  | boolean | false |
| axis          | Shows chart axis  | boolean | true |
| scale          | Scales up the chart. For example: scale 1/100 => `scale: 100` | number | - |
| heightMax          | Adjusts the height of the chart.  | number | - |
| palette          | Color palette of the chart `blues` `bussiness` `sunset`. [See more](#color-palettes)  | string | ubuntu |
| title          | Shows chart title  | string | - |
| titleFont          | Font of the title. Path to a typeface.json file or selector to `<a-asset-item>`. [See more](#fonts)  | JSON (list of objects) | [helvetiker_regular.typeface.json](https://rawgit.com/supermedium/superframe/master/components/text-geometry/lib/helvetiker_regular.typeface.json) |
| titleColor          | Color of the title  | string | #FFFFFF |
| titlePosition          | Position of the title  | string | 0 0 0 |
| animation          | Animates chart   | boolean | false |
| data          | Data to show with the chart  | JSON (list of objects) | - |

#### Data format
```json
[{"key":"David","key2":"2019","size":9},
{"key":"David","key2":"2018","size":8},
    ...
]
```

### Guides:
- [How to format data for BabiaXR](../../docs/how_to_format_data_to_babiaxr/)