---
linktitle: "babiaxr-querier-es"
title: "babiaxr-querier_es Component"
#date: 2020-09-23T15:08:39+02:00
draft: false
weight: 110
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

Component that will retrieve data from an ElasticSearch. It uses [ElasticSearch URI](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html) search to do it.

This component will put the data retrieved into the `babiaData` attribute of the entity.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/others/querier_elasticsearch_local/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ------ |
| elasticsearch_url             | Url of ElasticSearch  | string | - |
| index        | Index of the query | string   | - |
| size         | Size of the max results of the query | int   | 10 |
| query        | Query using the Lucene query string syntax, p.e: `q=name:dlumbrer`  | string   | - |

> **Important**: The ElasticSearch must have enabled cross-origin resource sharing because the queries are made by the browser. See this [ElasticSearch reference](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-http.html)

