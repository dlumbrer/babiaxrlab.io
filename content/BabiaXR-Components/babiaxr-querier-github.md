---
linktitle: "babiaxr-querier-github"
title: "babiaxr-querier_github Component"
#date: 2020-09-23T15:14:19+02:00
draft: false
weight: 130
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

Component that will retrieve data related to the repositories from an user using the GitHub API. It can be defined the username in order to get info about all the repositories or also it can be defined an array of repos in order to analyse just them (instead of all).

This component will put the data retrieved into the `babiaData` attribute of the entity.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/others/querier_github_all_repos/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| user            | GitHub username  | string | - |
| repos        | List of repo that you want to analyse | array   | (If empty it will retrieve all the repos of the user) |
