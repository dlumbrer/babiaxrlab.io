---
linktitle: "babiaxr-terrain"
title: "babiaxr-terrain Component"
#date: 2020-09-23T15:24:04+02:00
draft: false
weight: 180
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component creates a terrain using vertices data.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/elevation/elevation%20filled/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| width          | Width of the terrain  | number | 1 |
| height          | Height of the terrain  | number | 1 |
| segmentsWidth          | Number of width segments  | number | 1 |
| segmentsHeight          | Number of height segments  | number | 1 |
| color          | Color of the terrain  | string | #FFFFFF |
| filled          | Fill the terrain  | boolean | false |
| data          | Data about vertices of the terrain  | array | - |


#### Data Format
Data length must be the same as the vertices = segmentsWidth x segmentsHeight.

```
data:   0, 3, 6, 3, 0, 0, 2, 4, 2,
        0, 2, 0, 0, 2, 4, 6, 4, 2,

        ...

        0, 2, 0, 0, 2, 4, 6, 4, 2,
        0, 3, 6, 3, 0, 0, 2, 4, 2
```