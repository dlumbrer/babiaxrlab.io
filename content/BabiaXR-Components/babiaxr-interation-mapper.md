---
linktitle: "babiaxr-interation-mapper"
title: "babiaxr-interation-mapper Component"
#date: 2020-09-23T15:22:13+02:00
draft: false
weight: 170
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component just map events of an entity to others customizables.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/others/querier_json_embedded_debug/)

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| input            | Input event  | string | - |
| output            | Output event  | string | - |


### Data model

The dataset returned from the parsing of the `babiaxr-querier` components must has this model:

```
{
    "key1" : {
        "prop1": ["test" , "foo", "data"],
        "prop2": 12123,
        "prop3": "Data here",
        ...
    },
    "key2" : {
        ...
    }
    ...
}
```

