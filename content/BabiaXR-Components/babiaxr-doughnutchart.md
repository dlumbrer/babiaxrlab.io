---
#date: 2020-06-19T12:59:06+02:00
linktitle: babiaxr-doughnutchart
title: babiaxr-doughnutchart Component
draft: false
weight: 70
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component must be used with one of the [`babiaxr-vismapper`](../babiaxr-vismapper) components, with the `slice` and `size` attribute defined.

This component shows a doughnut chart.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts/doughnut_chart/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| legend          | Shows a legend when hovering a slice  | boolean | false |
| axis          | Shows chart axis  | boolean | true |
| palette          | Color palette of the chart `blues` `bussiness` `sunset`. [See more](#color-palettes)  | string | ubuntu |
| title          | Shows chart title  | string | - |
| titleFont          | Font of the title. Path to a typeface.json file or selector to `<a-asset-item>`. [See more](#fonts)  | JSON (list of objects) | [helvetiker_regular.typeface.json](https://rawgit.com/supermedium/superframe/master/components/text-geometry/lib/helvetiker_regular.typeface.json) |
| titleColor          | Color of the title  | string | #FFFFFF |
| titlePosition          | Position of the title  | string | 0 0 0 |
| animation          | Animates chart   | boolean | false |
| data          | Data to show with the chart  | JSON (list of objects) | - |

#### Data format
```json
[{"key":"kbn_network","size":10},
{"key":"Maria","size":5},
    ...
]
```
### Guides:
- [How to format data for BabiaXR](../../docs/how_to_format_data_to_babiaxr/)
