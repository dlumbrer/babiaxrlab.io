---
#date: 2020-06-19T12:58:40+02:00
linktitle: babiaxr-3dcylinderchart
title: babiaxr-3Dcylinderchart Component
draft: false
weight: 60
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component must be used with one of the [`babiaxr-vismapper`](../babiaxr-vismapper) components, with the `x-axis`, `z-axis`, `height` and `radius` attribute defined.

This component shows a 3D cylinder chart.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts/3dcylinder_chart/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| legend          | Shows a legend when hovering a cylinder  | boolean | false |
| axis          | Shows chart axis  | boolean | true |
| scale          | Scales up the chart. For example: scale 1/100 => `scale: 100` | number | - |
| heightMax          | Adjusts the height of the chart.  | number | - |
| radiusMax          | Adjusts bubbles' radius of the chart.  | number | - |
| palette          | Color palette of the chart `blues` `bussiness` `sunset`. [See more](#color-palettes)  | string | ubuntu |
| title          | Shows chart title  | string | - |
| titleFont          | Font of the title. Path to a typeface.json file or selector to `<a-asset-item>`. [See more](#fonts)  | JSON (list of objects) | [helvetiker_regular.typeface.json](https://rawgit.com/supermedium/superframe/master/components/text-geometry/lib/helvetiker_regular.typeface.json) |
| titleColor          | Color of the title  | string | #FFFFFF |
| titlePosition          | Position of the title  | string | 0 0 0 |
| animation          | Animates chart   | boolean | false |
| data          | Data to show with the chart  | JSON (list of objects) | - |

#### Data format
```json
[{"key":"David","key2":"2019","height":1,"radius":9},
{"key":"David","key2":"2018","height":2,"radius":8},
    ...
]
```
### Guides:
- [How to format data for BabiaXR](../../docs/how_to_format_data_to_babiaxr/)