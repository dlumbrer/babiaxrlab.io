---
linktitle: "babiaxr-totem"
title: "babiaxr-totem Component"
#date: 2020-09-23T14:47:03+02:00
draft: false
weight: 80
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component shows a totem in order to change the data of the visualizations that it's targeting.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/totems/totem_1/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| charts_id          | List of HTML IDs of the charts that will change their data  | JSON  stringfied (list of objects) | - |
| data_list          | List of datasets that the totem will switch  | JSON stringfied (list of objects) | - |

#### attributtes format

You can change the data of a `babiaxr-*chart` or a `babiaxr-vismapper` and retrieve the data from a JSON or querier.

- `charts_id`, define in "type" key if it a babiaxr-*chart or a vismapper:
    ```
    charts_id: [{"id": "pie1", "type": "babiaxr-piechart"}, {"id": "bars1", "type": "babiaxr-vismapper"}, ...]
    ```

- `data_list`, define a path of the JSON or a ID of the querier to get the data:
    ```
    data_list: [{"data":"Data 4", "from_querier": "<querier HTML id>"}, {"data": "Data 5", "path": "<json_path>"}, ...]
    ```

### Guides:
- [How to use Totem Component](../../docs/how_to_use_totem/)