---
linktitle: "babiaxr-navigation-bar"
title: "babiaxr-navigation-bar Component"
#date: 2020-09-23T14:56:16+02:00
draft: false
weight: 90
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component creates a timeline bar that changes a chart using an array of data. Also, let stop, skip, rewind and forward the time process. 

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/ui_nav_bar/simplebar_chart_with_ui/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| commits          | List of HTML IDs of the querier data and dates  | JSON  stringfied (list of objects) | - |
| size          | Size of the navigation bar   | number | 5 |
| points_by_line          | Number of points by line.    | number | 5 |
| to          | Direction of the process  | string | right |
| start_point          | Position of the starting point  | number | 0 |


```html
<a-entity id="datatest1" babiaxr-querier_json="url: ./data.json;"></a-entity>
<a-entity id="datatest2" babiaxr-querier_json="url: ./data2.json;"></a-entity>
    ...
<a-entity id="datatest6" babiaxr-querier_json="url: ./data6.json;"></a-entity>

<a-entity id="navigationbar" babiaxr-navigation-bar =
       'commits: [{"date": "01/30/2003", "commit": "datatest1"}, 
                  {"date": "02/17/2003", "commit": "datatest2"},
                    ...
                  {"date": "01/13/2004", "commit": "datatest6"}]'>
</a-entity>
```
