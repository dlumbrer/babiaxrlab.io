---
linktitle: "babiaxr-querier-json"
title: "babiaxr-querier_json Component"
#date: 2020-09-23T15:11:34+02:00
draft: false
weight: 120
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

Component that will retrieve data from a JSON input that can be defined as an url or directly embedded.

This component will put the data retrieved into the `babiaData` attribute of the entity.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts_querier/simplebar_chart_querier/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ------ |
| url             | Url of the file with the JSON data  | string | - |
| embedded        | JSON data directly stringified in the property | string   | - |

### Guides:
- [How to use querier/filter/vismapper components](../../docs/how_to_use_querier_filter_vismapper/)