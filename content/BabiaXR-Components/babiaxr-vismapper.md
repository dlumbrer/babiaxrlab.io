---
linktitle: "babiaxr-vismapper"
title: "babiaxr-vismapper Component"
#date: 2020-09-23T15:18:08+02:00
draft: false
weight: 150
categories: [ "Components", "Documentation" ]
tags: ["api", "demo"]
---

This component map the data selected by the [`babiaxr-filterdata`](../babiaxr-filterdata) component into a chart component or physical properties of a geometry component.

This component must be in the same entity than filterdata and it needs also a chart component or geometry.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts_querier/simplebar_chart_querier/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| ui            | Display a UI selector to select what will be mapped. | bolean | `false`|
| height        | Field of the data selected by filterdata that will be mapped as the height of the items of the charts or a geometry. Valid for [**babiaxr-3dbarchart**](../babiaxr-3dbarchart), [**babiaxr-cylinderchart**](../babiaxr-cylinderchart), [**babiaxr-3dcylinderchart**](../babiaxr-3dcylinderchart), [**babiaxr-bubbleschart**](../babiaxr-bubbleschart), [**babiaxr-simplebarchart**](../babiaxr-simplebarchart) and **box/sphere** | string   | - |
| radius        | Field of the data selected by filterdata that will be mapped as the radius of the items of the charts or a geometry. Valid for [**babiaxr-cylinderchart**](../babiaxr-cylinderchart), [**babiaxr-3dcylinderchart**](../babiaxr-3dcylinderchart), [**babiaxr-bubbleschart**](../babiaxr-bubbleschart) and **sphere** | string   | - |
| slice        | Field of the data selected by filterdata that will be mapped as the slices of the items of a pie chart. Valid for [**babiaxr-piechart**](../babiaxr-piechart) and [**babiaxr-doughnutchart**](../babiaxr-doughnutchart) | string   | - |
| z-axis        | Field of the data selected by filterdata that will be mapped as the keys of the Z Axis of the chart component selected. Valid for [**babiaxr-3dbarchart**](../babiaxr-3dbarchart), [**babiaxr-bubbleschart**](../babiaxr-bubbleschart) and [**babiaxr-3dcylinderchart**](../babiaxr-3dcylinderchart) | string   | - |
| x-axis        | Field of the data selected by filterdata that will be mapped as the keys of the X Axis of the chart component selected. Valid for [**babiaxr-3dbarchart**](../babiaxr-3dbarchart), [**babiaxr-3dcylinderchart**](../babiaxr-3dcylinderchart), [**babiaxr-bubbleschart**](../babiaxr-bubbleschart) and [**babiaxr-simplebarchart**](../babiaxr-simplebarchart) | string   | - |
| width            | Field of the data selected by filterdata that will be mapped as the width of the geometry **(only for box geometry)**.  | string | - |
| depth        | Field of the data selected by filterdata that will be mapped as the depth of the geometry **(only for box geometry)**. | string   | - |

### Guides:
- [How to use querier/filter/vismapper components](../../docs/how_to_use_querier_filter_vismapper/)