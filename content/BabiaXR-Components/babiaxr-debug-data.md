---
linktitle: "babiaxr-debug-data"
title: "babiaxr-debug-data Component"
#date: 2020-09-23T15:20:34+02:00
draft: false
weight: 160
categories: [ "Components", "Documentation" ]
tags: ["api", "demo"]
---

This component force the entity to hear an event, when this event occurs, a debug plane with the data of the `babiaData` entity attribute will appear in the position (or close) of the entity that it belongs.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/others/querier_json_embedded_debug/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| inputEvent            | Name of the event that will be hearing  | string | - |

