---
title: "Release Notes 1.0.10"
date: 2020-07-07T13:42:20+02:00
draft: false
---

### Detective Conan

> NPM 1.0.10 version

- Main page of babiaXR updated (with a blog!).
- Demo for VISSOFT paper prepared (https://thesis-dlumbrer.gitlab.io/vissoft2020/).
- UI Navbar now supports big time points (defining the points per line).
- UI Navbar supports multiple components aiming.
