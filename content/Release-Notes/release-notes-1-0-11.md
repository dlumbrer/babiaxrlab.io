---
title: "Release Notes 1.0.11"
date: 2020-10-22T13:42:20+02:00
draft: false
---

### Haikyu!!

> NPM 1.0.11 version

- Changed the name of the components to `babiaxr- `.
- Created vismapper's interface for **Oculus Quest**.
- Added new demo with all updates until now.
- Fixed raycaster bugs.
- Updated the [website](https://babiaxr.gitlab.io) with new guides and information about the BabiaXR components.