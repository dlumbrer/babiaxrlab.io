---
title: "Release Notes 1.1.0"
date: 2021-02-12T10:10:20+02:00
draft: false
---

### Kakegurui

> NPM 1.1.0 version

- New STACK (see [STACK.md](https://gitlab.com/babiaxr/aframe-babia-components/-/blob/master/docs/others/STACK.md))
- Al the documentation updated with the islands and city visualizations and the new way to build scenes.