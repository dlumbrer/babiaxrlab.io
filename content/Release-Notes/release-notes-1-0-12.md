---
title: "Release Notes 1.0.12"
date: 2020-11-05T10:42:20+02:00
draft: false
---

### Inuyasha

> NPM 1.0.12 version

- Added testing for the components
- First iteraction of adding the vismapper component to codecity
- [Demo with screenshots](https://babiaxr.gitlab.io/aframe-babia-components/).