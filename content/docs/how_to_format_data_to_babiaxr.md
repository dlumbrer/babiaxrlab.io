---
linktitle: "guide_format_data"
date: 2020-08-26T12:40:05+02:00
title: How to format data to BabiaXR 
draft: false
categories: [ "Tutorials", "Documentation" ]
tags: ["api", "data format", "guide"]
---

Let's learn how to format data in order to use them with babiaxr components.

Before we start, we have to know the needed data for each component. You can find it in babiaxr general docs. In summary:

- **babiaxr-piechart**: key, size
- **babiaxr-simplebartchart**: key, size 
- **babiaxr-3dbarchart**: key, key2, size
- **babiaxr-bubbleschart**: key, key2, height, radius
- **babiaxr-cylinderchart**: key, height, radius
- **babiaxr-3dcylinderchart**: key, key2, height, radius
- **babiaxr-doughnutchart**: key, size


Once we have chosen a component, there are several ways to introduce the data:

- Manually inside the component
- Using external JSON files

## Insert Data manually

If we want to introduce the data directly, we need to follow the format given in the Babia documentation.

For example: we want to draw the `babiaxr-bubbleschart` component.

The required data for this component are: `key`, `key2`, `height` and `radius`.

So our data must have the following format:

```
[{
    "key":"David",
    "key2":"2019",
    "height":1,
    "radius":9
},
{
    "key":"David",
    "key2":"2018",
    "height":2,
    "radius":8
},
...
]
```

As you can see this is an object array, each of them represents the bubble values respectively.

If any of this fields is missing the component won't work.

On the other side, we can add more data to the component:

```
[{
    "key":"David",
    "key2":"2019",
    "height":1,
    "radius":9,
    "size": 100
},
...
]
```
the `babiaxr-bubbleschart` component will work but it won't use the `size` value


## External JSON Data

In case we want to have the data in external files outside HTML, first we need to import them using `babiaxr-querier_json`. Then wee add them to the babia component with `babiaxr-filterdata` and `babiaxr-vismapper`.

For the moment we'll focus on data formatting instead of how to import it.

The advantage of this method is the data customization.

### Normal Data

This is very similar to manual method but with a change:

```
{
  "0": {
    "key": "David",
    "key2": "2019",
    "height": 9,
    "radius": 1
  },
  "1a": {
    "key": "David",
    "key2": "2018",
    "height": 8,
    "radius": 2
  },
  "02": {
    "key": "David",
    "key2": "2017",
    "height": 7,
    "radius": 3
  },
...
}
```

In this case we have an object that contains objects. Each of them represents a `babiaxr-bubbleschart` component.

>Tip: It's not necessary using ordered keys or enumerations, but it keeps the example easy.

### Custom Data

You may be thinking, what if I don't want to use keys like `key`, `key2`, `radius`, etc... Then this is your best choice:

As we previously mentioned, this option allows us to customize the data.

For example, we want to represent the same data but changing the key names in order to identify them better:

- key => name
- key2 => year
- height => projects
- radius => pending

```
{
  "0": {
    "name": "David",
    "year": "2019",
    "projects": 9,
    "pending": 1
  },
  "1a": {
    "name": "David",
    "year": "2018",
    "projects": 8,
    "pending": 2
  },
  "02": {
    "name": "David",
    "year": "2017",
    "projects": 7,
    "pending": 3
  },
...
}
```

>Note: You'll need to know how to use the [`babiaxr-vismapper`](../how_to_use_querier_filter_vismapper#vismapper) component to make this work.