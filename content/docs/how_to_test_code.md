---
title: How to test code in BabiaXR
date: 2020-07-22T12:10:12+02:00
draft: false
categories: [ "Documentation" ]
tags: ["testing", "test"]
---

Currently, we use `karma` testing runner.

In order to test code in this project, just create a testing file into the directory `/tests` using `mocha`, `chai` and `sinon` syntax. These files must be named like:
```
YOUR_TEST.test.js
```

To start testing, execute the next command:
```
npm run test
```

If you want to test only one browser (`firefox` or `chrome`):
```
npm run test:firefox
```
or 
```
npm run test:chrome
```


Read more about contributing with us: https://gitlab.com/babiaxr/aframe-babia-components/-/blob/master/docs/CONTRIBUTING.md



Links:

- [Karma](https://karma-runner.github.io/5.0/index.html)
- [MochaJS](https://mochajs.org)
- [ChaiJS](https://www.chaijs.com)
- [SinonJS for Chai](https://www.chaijs.com/plugins/sinon-chai/)

