---
linktitle: "guide_querier_filter_mapper"
date: 2020-08-26T17:32:05+02:00
title: How to use Querier/Filter/Mapper 
draft: false
categories: [ "Tutorials", "Documentation" ]
tags: ["api", "components", "guide", "demo"]
---

>[Remix this example on Glitch](https://glitch.com/~example-querier). Or [view the demo](https://example-querier.glitch.me).

In this tutorial we are learning how to use `babiaxr-querier_json`, `babiaxr-filterdata`y `babiaxr-vismapper` components.

Let's define each component purpose:

- **babiaxr-querier_json**: it imports external JSON files.
- **babiaxr-filterdata**: it filters the data from the file.
- **babiaxr-vismapper**: it takes and maps the result from `babiaxr-filterdata` in order to represent them.

![image](https://gitlab.com/babiaxr/aframe-babia-components/uploads/f49795d4f0cddcca9aedc00f3afe2d18/Captura_de_pantalla_2020-07-27_a_las_12.57.49.png)

As we can see in the diagram, the `querier_json` component is inside an independent entity. Meanwhile `babiaxr-filterdata` and `babiaxr-vismapper` are in the same entity of babiaxr-component.

For this tutorial we are adding a `babiaxr-simplebarchart` to the scene.

First we create our scene:

```
<!DOCTYPE html>
<html>
  <head>
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-babia-components/dist/aframe-babia-components.min.js"></script>
  </head>

  <body>
    <a-scene>

    </a-scene>
  </body>
</html>
```

## Querier JSON

As we mentioned before, the component `babiaxr-querier_json` will import JSON files with the data.

We need a JSON file with data, let's use [this](https://gitlab.com/babiaxr/aframe-babia-components/-/raw/master/examples/charts_querier/simplebar_chart_querier/data.json). You can use other files if you want.

>Note: If you want to know how to create data files for BabiaXR check [this guide](../how_to_format_data_to_babiaxr/).

For now, we want to know just the file url. Also we'll give an identificator to the component.

```
<a-entity id="data" babiaxr-querier_json="url: ./data.json;"></a-entity>
```

## Filterdata

Now that we have the JSON file imported, we are adding `babiaxr-simplebarchart` and `babiaxr-filterdata` components.

`babiaxr-filterdata` prepares the data and filters in order to map them later. We need to add the `from` attribute with our identificator (same as `babiaxr-querier_json`) to obtain the file.

```
<a-entity babiaxr-simplebarchart='legend: true; axis: true' babiaxr-filterdata="from: data" ></a-entity>
```

In this case (no filter specified), it will return all the data.

If we want to filter (ex. only data with name David) we need to use `filter` attribute.

```
<a-entity babiaxr-simplebarchart='legend: true; axis: true' babiaxr-filterdata="from: data; filter: name=David" ></a-entity>
```
It returns elements with name = David.

## Vismapper

Last we'll add `babiaxr-vismapper` component, which will map our data to `babiaxr-simplebarchart` component format.

This guide shows each component properties [link](https://gitlab.com/babiaxr/aframe-babia-components#vismapper-component)

In this example, babiaxr-simplebarchart expets the data types: `height` and `x_axis`; but in our JSON we have: `name`, `name2`, `size` and `height`.

- `height` numeric type.
- `x_axis` it represents tags (string type but it can be a number).

We can map them this way:

- height => size
- x_axis => name

Then we have as result:
```
<a-entity babiaxr-simplebarchart='legend: true; axis: true' babiaxr-filterdata="from: data; filter: name=David" babiaxr-vismapper='x_axis: name; height: size' ></a-entity>
```