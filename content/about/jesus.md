---
name: "Jesús M. González Barahona"
weight: 20
draft: false
avatar: "https://gsyc.urjc.es/~jgb/jgb.jpg"
github : "jgbarah"
gitlab : "jgbarah"
linkedin: "jgbarah"
twitter: "jgbarah"
mail: ""
---
Professor at University Rey Juan Carlos